import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=query, headers=headers)
    # setting the value to content and jsonloading it
    # you can replace this with content = response.json()
    content = json.loads(response.content)
    # in case the value is not there because this will throw the entire page
    try:
        # the data is a list of dictionary so you want to pull the [0]
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather_data(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url).json()
    lat = geo_response[0]["lat"]
    lon = geo_response[0]["lon"]

    query = f"lat={lat}&lon={lon}"
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?{query}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url).json()

    try:
        weather = {
            "temp": weather_response["main"]["temp"],
            "description": weather_response["weather"][0]["description"]
        }
        return {"weather": weather}
    except (KeyError, IndexError):
        return {"weather": None}
